PRODUCT_PACKAGES += \
    SettingsDark \
    SystemDark \
    SystemUIDark 

# Accents
PRODUCT_PACKAGES += \
    BrownAccent \
    TealAccent \
    BlackAccent \
    YellowAccent \
    RedAccent \
    PurpleAccent \
    PinkAccent \
    OrangeAccent \
    GreenAccent \
    WhiteAccent \
    CyanAccent \

# Accents from famous brands
PRODUCT_PACKAGES += \
    AospaGreen \
    AndroidOneGreen \
    CocaColaRed \
    DiscordPurple \
    FacebookBlue \
    InstagramCerise \
    JollibeeCrimson \
    MonsterGreen \
    NextbitMint \
    OnePlusRed \
    PepsiBlue \
    PocoYellow \
    RazerGreen \
    SamsungBlue \
    SpotifyGreen \
    StarbucksGreen \
    TwitchPurple \
    TwitterBlue \
    XboxGreen \
    XiaomiOrange

